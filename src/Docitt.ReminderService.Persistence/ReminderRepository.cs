﻿using Docitt.ReminderService.Services;
using LendFoundry.Foundation.Persistence.Mongo;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.ReminderService.Persistence
{
    public class ReminderRepository : MongoRepository<IReminderModel, ReminderModel>, IReminderRepository
    {

        static ReminderRepository()
        {
            BsonClassMap.RegisterClassMap<ReminderModel>(map =>
            {
                map.AutoMap();
                var type = typeof(ReminderModel);
                map.MapMember(m => m.CreatedDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public ReminderRepository(LendFoundry.Tenant.Client.ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "reminders")
        {
            CreateIndexIfNotExists("entity-type", Builders<IReminderModel>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType));
            CreateIndexIfNotExists("entity-type-id", Builders<IReminderModel>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType).Ascending(i => i.EntityId));
        }

        public async Task<IEnumerable<IReminderModel>> GetAll(string entityType, string entityId, string userName)
        {
            if (!string.IsNullOrWhiteSpace(userName))
                return await Query.Where(p => p.EntityType == entityType && p.EntityId == entityId && p.UserName == userName).ToListAsync();
            else
                return await Query.Where(p => p.EntityType == entityType && p.EntityId == entityId).ToListAsync();
        }

        public async Task<IEnumerable<IReminderModel>> GetByDate(string entityType, string entityId, string userName, DateTimeOffset date)
        {
            if (!string.IsNullOrWhiteSpace(userName))
                return await Query.Where(p => p.EntityType == entityType && p.EntityId == entityId && p.UserName == userName && p.CreatedDate == date).ToListAsync();
            else
                return await Query.Where(p => p.EntityType == entityType && p.EntityId == entityId && p.CreatedDate == date).ToListAsync();
        }

        public async Task<IEnumerable<IReminderModel>> GetUnReadReminderByDate(string entityType, string entityId, string userName, DateTimeOffset date)
        {
            if (!string.IsNullOrWhiteSpace(userName))
                return await Query.Where(p => p.EntityType == entityType && p.EntityId == entityId && p.UserName == userName && !p.isRead && p.CreatedDate == date).ToListAsync();
            else
                return await Query.Where(p => p.EntityType == entityType && p.EntityId == entityId && !p.isRead && p.CreatedDate == date).ToListAsync();
        }

        public async Task<IEnumerable<IReminderModel>> GetUnReadReminder(string entityType, string entityId, string userName)
        {
            if (!string.IsNullOrWhiteSpace(userName))
                return await Query.Where(p => p.EntityType == entityType && p.EntityId == entityId && p.UserName == userName && !p.isRead).ToListAsync();
            else
                return await Query.Where(p => p.EntityType == entityType && p.EntityId == entityId && !p.isRead).ToListAsync();
        }

        public async void MarkAsRead(string entityType, string entityId, string userName, IEnumerable<string> reminderIds)
        {
            if (!string.IsNullOrWhiteSpace(userName))
            {
                var filter = Builders<IReminderModel>.Filter.Where(x => x.EntityType == entityType && x.EntityId == entityId && x.UserName == userName && reminderIds.Contains(x.Id));
                var update = Builders<IReminderModel>.Update.Set(x => x.isRead, true);
                await Collection.UpdateManyAsync(filter, update);
            }
            else
            {
                var filter = Builders<IReminderModel>.Filter.Where(x => x.EntityType == entityType && x.EntityId == entityId && reminderIds.Contains(x.Id));
                var update = Builders<IReminderModel>.Update.Set(x => x.isRead, true);
                await Collection.UpdateManyAsync(filter, update);
            }
        }

        public async void MarkAllAsRead(string entityType, string entityId, string userName)
        {
            if (!string.IsNullOrWhiteSpace(userName))
            {
                var filter = Builders<IReminderModel>.Filter.Where(x => x.EntityType == entityType && x.EntityId == entityId && x.UserName == userName && !x.isRead);
                var update = Builders<IReminderModel>.Update.Set(x => x.isRead, true);
                await Collection.UpdateManyAsync(filter, update);
            }
            else
            {
                var filter = Builders<IReminderModel>.Filter.Where(x => x.EntityType == entityType && x.EntityId == entityId && !x.isRead);
                var update = Builders<IReminderModel>.Update.Set(x => x.isRead, true);
                await Collection.UpdateManyAsync(filter, update);
            }
        }

        public async Task<IEnumerable<IReminderModel>> GetAll(string userName)
        {
            return await Query.Where(p => p.UserName == userName).ToListAsync();
        }

        public async Task<IEnumerable<IReminderModel>> GetByDate(string userName, DateTimeOffset date)
        {
            return await Query.Where(p => p.UserName == userName && p.CreatedDate == date).ToListAsync();
        }

        public async Task<IEnumerable<IReminderModel>> GetUnReadReminder(string userName)
        {
            return await Query.Where(p => p.UserName == userName && !p.isRead).ToListAsync();
        }

        public async void MarkAsRead(string userName, IEnumerable<string> reminderIds)
        {
            var filter = Builders<IReminderModel>.Filter.Where(x => x.UserName == userName && reminderIds.Contains(x.Id));
            var update = Builders<IReminderModel>.Update.Set(x => x.isRead, true);
            await Collection.UpdateManyAsync(filter, update);
        }

        public async void MarkAllAsRead(string userName)
        {
            var filter = Builders<IReminderModel>.Filter.Where(x => x.UserName == userName && !x.isRead);
            var update = Builders<IReminderModel>.Update.Set(x => x.isRead, true);
            await Collection.UpdateManyAsync(filter, update);
        }
    }
}