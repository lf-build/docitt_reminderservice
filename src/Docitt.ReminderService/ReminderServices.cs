﻿using Docitt.ReminderService.Services;
using LendFoundry.EventHub;

using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Docitt.ReminderService
{
    public class ReminderServices : IReminderService
    {
        public ReminderServices
        (
            IReminderRepository repository,
            ITenantTime TenantTime,
            IEventHubClient eventhub,
            ILookupService lookup
        )
        {
            if (repository == null) throw new ArgumentException($"{nameof(repository)} is mandatory");
            if (TenantTime == null) throw new ArgumentException($"{nameof(TenantTime)} is mandatory");
            if (eventhub == null) throw new ArgumentException($"{nameof(eventhub)} is mandatory");
            if (lookup == null) throw new ArgumentException($"{nameof(lookup)} is mandatory");
            Lookup = lookup;
            Repository = repository;
            tenantTime = TenantTime;
            EventHub = eventhub;
        }
        private ILookupService Lookup { get; }
        private IEventHubClient EventHub { get; }

        private ITenantTime tenantTime { get; }

        private IReminderRepository Repository { get; }
        public async Task Add(string entityType, string entityId, string userName, IRequestPayload reminderData)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory");

            EnsureDataIsValid(reminderData);

            entityType = EnsureEntityTypeIsValid(entityType);

            IReminderModel objReminder = new ReminderModel
            {
                CreatedDate = tenantTime.Now,
                EntityType = entityType,
                EntityId = entityId,
                Description = reminderData.Description
            };

            if (!string.IsNullOrWhiteSpace(userName))
                objReminder.UserName = WebUtility.UrlDecode(userName);

            Repository.Add(objReminder);

            await EventHub.Publish($"{UppercaseFirst(entityType)}{nameof(Events.ReminderRequestCreated)}", new Events.ReminderRequestCreated { Request = objReminder });
        }

        public async Task<IEnumerable<IReminderModel>> GetAll(string entityType, string entityId, string userName)
        {
            EnsureEntityTypeIsValid(entityType);

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory");

            if (!string.IsNullOrWhiteSpace(userName))
                userName = WebUtility.UrlDecode(userName);

            return await Repository.GetAll(entityType, entityId, userName);
        }

        public async Task<IEnumerable<IReminderModel>> GetByDate(string entityType, string entityId, string userName, DateTimeOffset date)
        {
            EnsureEntityTypeIsValid(entityType);

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory");
            var dateTimeOffset = tenantTime.FromDate(date.Date);

            if (!string.IsNullOrWhiteSpace(userName))
                userName = WebUtility.UrlDecode(userName);

            return await Repository.GetByDate(entityType, entityId, userName, dateTimeOffset);
        }

        public async Task<IEnumerable<IReminderModel>> GetUnReadReminderByDate(string entityType, string entityId, string userName, DateTimeOffset date)
        {
            EnsureEntityTypeIsValid(entityType);

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory");

            var dateTimeOffset = tenantTime.FromDate(date.Date);

            if (!string.IsNullOrWhiteSpace(userName))
                userName = WebUtility.UrlDecode(userName);

            return await Repository.GetUnReadReminderByDate(entityType, entityId, userName, dateTimeOffset);
        }

        public async Task<IEnumerable<IReminderModel>> GetUnReadReminder(string entityType, string entityId, string userName)
        {
            EnsureEntityTypeIsValid(entityType);

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory");

            if (!string.IsNullOrWhiteSpace(userName))
                userName = WebUtility.UrlDecode(userName);

            return await Repository.GetUnReadReminder(entityType, entityId, userName);
        }

        public async Task<int> GetUnReadReminderCount(string entityType, string entityId, string userName)
        {
            EnsureEntityTypeIsValid(entityType);

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory");

            if (!string.IsNullOrWhiteSpace(userName))
                userName = WebUtility.UrlDecode(userName);

            var result = await Repository.GetUnReadReminder(entityType, entityId, userName);
            return result.Count();
        }

        public async Task<IEnumerable<IReminderModel>> MarkAsRead(string entityType, string entityId, string userName, IEnumerable<string> reminderIds)
        {
            EnsureEntityTypeIsValid(entityType);

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory");

            if (!reminderIds.Any())
                throw new ArgumentException($"{nameof(reminderIds)} is mandatory");

            if (!string.IsNullOrWhiteSpace(userName))
                userName = WebUtility.UrlDecode(userName);

            Repository.MarkAsRead(entityType, entityId, userName, reminderIds);
            IReminderModel reminderModel = new ReminderModel()
            {
                EntityId = entityId,
                EntityType = entityType
            };

            var reminders = await Repository.GetUnReadReminder(entityType, entityId, userName);
            await EventHub.Publish($"{UppercaseFirst(entityType)}{nameof(Events.ReminderRequestCompleted)}", new Events.ReminderRequestCompleted { Request = reminderModel });
            return reminders;
        }

        public async Task MarkAllAsRead(string entityType, string entityId, string userName)
        {
            EnsureEntityTypeIsValid(entityType);

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory");

            if (!string.IsNullOrWhiteSpace(userName))
                userName = WebUtility.UrlDecode(userName);

            Repository.MarkAllAsRead(entityType, entityId, userName);
            IReminderModel reminderModel = new ReminderModel()
            {
                EntityId = entityId,
                EntityType = entityType
            };

          await  EventHub.Publish($"{UppercaseFirst(entityType)}{nameof(Events.ReminderRequestCompleted)}", new Events.ReminderRequestCompleted { Request = reminderModel });
        }

        public async Task<IEnumerable<IReminderModel>> GetAll(string userName)
        {
            if (!string.IsNullOrWhiteSpace(userName))
                userName = WebUtility.UrlDecode(userName);

            return await Repository.GetAll(userName);
        }

        public async Task<IEnumerable<IReminderModel>> GetByDate(string userName, DateTimeOffset date)
        {
            var dateTimeOffset = tenantTime.FromDate(date.Date);

            if (!string.IsNullOrWhiteSpace(userName))
                userName = WebUtility.UrlDecode(userName);

            return await Repository.GetByDate(userName, dateTimeOffset);
        }

        public async Task<IEnumerable<IReminderModel>> GetUnReadReminder(string userName)
        {

            if (!string.IsNullOrWhiteSpace(userName))
                userName = WebUtility.UrlDecode(userName);

            return await Repository.GetUnReadReminder(userName);
        }

        public async Task< int> GetUnReadReminderCount(string userName)
        {
            if (!string.IsNullOrWhiteSpace(userName))
                userName = WebUtility.UrlDecode(userName);
           var result = await Repository.GetUnReadReminder(userName);
            return result.Count();  
        }

        public async Task<IEnumerable<IReminderModel>> MarkAsRead(string userName, IEnumerable<string> reminderIds)
        {
            if (!reminderIds.Any())
                throw new ArgumentException($"{nameof(reminderIds)} is mandatory");

            if (!string.IsNullOrWhiteSpace(userName))
                userName = WebUtility.UrlDecode(userName);

            Repository.MarkAsRead(userName, reminderIds);

            List<ReminderModel> objReminderModel = new List<ReminderModel>();
            foreach (var reminderid in reminderIds)
            {
                var reminder = Repository.Get(reminderid).Result;
                if (!objReminderModel.Any(x => x.EntityType == reminder.EntityType && x.EntityId == reminder.EntityId))
                {
                    var reminderModel = new ReminderModel()
                    {
                        EntityId = reminder.EntityId,
                        EntityType = reminder.EntityType
                    };
                    objReminderModel.Add(reminderModel);
                    await EventHub.Publish($"{UppercaseFirst(reminderModel.EntityType)}{nameof(Events.ReminderRequestCompleted)}", new Events.ReminderRequestCompleted { Request = reminderModel });
                }
            }

            objReminderModel = null;

            var reminders = await Repository.GetUnReadReminder(userName);
            //await EventHub.Publish($"{UppercaseFirst(entityType)}{nameof(Events.ReminderRequestCompleted)}", new Events.ReminderRequestCompleted { Request = reminderModel });
            return reminders;
        }

        public async Task MarkAllAsRead(string userName)
        {
            if (!string.IsNullOrWhiteSpace(userName))
                userName = WebUtility.UrlDecode(userName);

            var reminderids = Repository.GetUnReadReminder(userName).Result;

            Repository.MarkAllAsRead(userName);

            if (reminderids.Any())
            {
                List<ReminderModel> objReminderModel = new List<ReminderModel>();
                foreach (var reminderid in reminderids)
                {
                    if (objReminderModel.FirstOrDefault(x => x.EntityType == reminderid.EntityType && x.EntityId == reminderid.EntityId) == null)
                    {
                        var reminderModel = new ReminderModel()
                        {
                            EntityId = reminderid.EntityId,
                            EntityType = reminderid.EntityType
                        };
                        objReminderModel.Add(reminderModel);
                       await   EventHub.Publish($"{UppercaseFirst(reminderModel.EntityType)}{nameof(Events.ReminderRequestCompleted)}", new Events.ReminderRequestCompleted { Request = reminderModel });
                    }
                }

                objReminderModel = null;
            }
        }

        private void EnsureDataIsValid(IRequestPayload payload)
        {
            if (payload == null)
                throw new ArgumentException($"{nameof(payload)} is mandatory");

            if (string.IsNullOrEmpty(payload.Description) || string.IsNullOrWhiteSpace(payload.Description))
                throw new ArgumentException($"{nameof(payload.Description)} is mandatory");
        }

        private string EnsureEntityTypeIsValid(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory");

            entityType = entityType.ToLower();
            if (Lookup.GetLookupEntry("entityTypes", entityType) == null)
                throw new ArgumentException($"{entityType} is not a valid entity");
            return entityType;
        }

        private static string UppercaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s)) return string.Empty;
            return $"{char.ToUpper(s[0])}{s.Substring(1)}";
        }
    }
}
