﻿using Docitt.ReminderService.Services;
using LendFoundry.Foundation.Services;
using System;
using System.Threading.Tasks;
using LendFoundry.Foundation.Logging;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using System.Collections.Generic;
namespace Docitt.ReminderService.Api.Controllers
{
    /// <summary>
    /// Represents API controller class.
    /// </summary>
    public class ApiController : ExtendedController
    {
        /// <summary>
        /// Represents constructor class.
        /// </summary>
        /// <param name="service"></param>
        /// <param name="logger"></param>
        public ApiController(IReminderService service, ILogger logger) : base(logger)
        {
            if (service == null)
                throw new ArgumentException($"{nameof(service)} is mandatory");

            Service = service;
            ErrorLog = logger;
        }

        private IReminderService Service { get; }

        private static NoContentResult NoContentResult { get; } = new NoContentResult();

        private ILogger ErrorLog { get; }
        /// <summary>
        /// Add Reminder
        /// </summary>
        /// <param name="entityId">entityId</param>
        /// <param name="entityType">entityType</param>
        /// <param name="request">request</param>
        /// <returns>IApplicant</returns>
        [HttpPost("/{entityType}/{entityId}")]
#if DOTNET2
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> Add(string entityType, string entityId, [FromBody]RequestPayload request)
        {
            try
            {
                await   Service.Add(entityType, entityId, null, request);
                return NoContentResult;;
            }
            catch (ArgumentException exception)
            {
                ErrorLog.Warn(exception.Message, new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (Exception ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Add Reminder
        /// </summary>
        /// <param name="entityId">entityId</param>
        /// <param name="entityType">entityType</param>
        /// <param name="userName">userName</param>
        /// <param name="request">request</param>
        /// <returns>IApplicant</returns>
        [HttpPost("/{entityType}/{entityId}/{userName}")]
#if DOTNET2
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif

        public async Task<IActionResult> Add(string entityType, string entityId, string userName, [FromBody]RequestPayload request)
        {
            try
            {
                await  Service.Add(entityType, entityId, userName, request);
                return NoContentResult; ;
            }
            catch (ArgumentException exception)
            {
                ErrorLog.Warn(exception.Message, new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (Exception ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Get All Reminder
        /// </summary>
        /// <param name="entityId">entityId</param>
        /// <param name="entityType">entityType</param>
        /// <returns>IReminderModel[]</returns>
        [HttpGet("/{entityType}/{entityId}")]
#if DOTNET2
        [ProducesResponseType(typeof(IEnumerable< IReminderModel>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> GetAll(string entityType, string entityId)
        {
            try
            {
                return Ok(await Service.GetAll(entityType, entityId, null));
            }
            catch (ArgumentException exception)
            {
                ErrorLog.Warn(exception.Message, new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (Exception ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Get All Reminder
        /// </summary>
        /// <param name="entityId">entityId</param>
        /// <param name="entityType">entityType</param>
        /// <param name="userName">userName</param>
        /// <returns>IReminderModel[]</returns>
        [HttpGet("/{entityType}/{entityId}/{userName}")]
#if DOTNET2
        [ProducesResponseType(typeof(IEnumerable<IReminderModel>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> GetAll(string entityType, string entityId, string userName)
        {
            try
            {
                return Ok(await Service.GetAll(entityType, entityId, userName));
            }
            catch (ArgumentException exception)
            {
                ErrorLog.Warn(exception.Message, new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (Exception ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }

        }

        /// <summary>
        /// GetByDate
        /// </summary>
        /// <param name="entityId">entityId</param>
        /// <param name="entityType">entityType</param>
        /// <param name="date">date</param>
        /// <returns>IReminderModel[]</returns>
        [HttpGet("/{entityType}/{entityId}/date/{date}")]
#if DOTNET2
        [ProducesResponseType(typeof(IEnumerable<IReminderModel>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> GetByDate(string entityType, string entityId, string date)
        {
            try
            {
                if (string.IsNullOrEmpty(date))
                    throw new ArgumentException($"{nameof(date)} is mandatory");

                DateTimeOffset dateoffset;
                if (!DateTimeOffset.TryParse(date, out dateoffset))
                {
                    throw new ArgumentException($"{nameof(date)} is not in proper format");
                }

                return Ok(await Service.GetByDate(entityType, entityId, null, dateoffset));
            }
            catch (ArgumentException exception)
            {
                ErrorLog.Warn(exception.Message, new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (Exception ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }

        }

        /// <summary>
        /// GetByDate
        /// </summary>
        /// <param name="entityId">entityId</param>
        /// <param name="entityType">entityType</param>
        /// <param name="userName">userName</param>
        /// <param name="date">date</param>
        /// <returns>IReminderModel[]</returns>
        [HttpGet("/{entityType}/{entityId}/{userName}/date/{date}")]
#if DOTNET2
        [ProducesResponseType(typeof(IEnumerable< IReminderModel>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async  Task<IActionResult> GetByDate(string entityType, string entityId, string userName, string date)
        {
            try { 
            if (string.IsNullOrEmpty(date))
                throw new ArgumentException($"{nameof(date)} is mandatory");

            DateTimeOffset dateoffset;
            if (!DateTimeOffset.TryParse(date, out dateoffset))
            {
                throw new ArgumentException($"{nameof(date)} is not in proper format");
            }

            return  Ok(await Service.GetByDate(entityType, entityId, userName, dateoffset));
            }
            catch (ArgumentException exception)
            {
                ErrorLog.Warn(exception.Message, new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (Exception ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }

        }

        /// <summary>
        /// GetUnReadReminderByDate
        /// </summary>
        /// <param name="entityId">entityId</param>
        /// <param name="entityType">entityType</param>
        /// <param name="date">date</param>
        /// <returns>IReminderModel[]</returns>
        [HttpGet("/{entityType}/{entityId}/unread/date/{date}")]
#if DOTNET2
        [ProducesResponseType(typeof(IEnumerable<IReminderModel>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async  Task<IActionResult> GetUnReadReminderByDate(string entityType, string entityId, string date)
        {
            try
            {
                if (string.IsNullOrEmpty(date))
                    throw new ArgumentException($"{nameof(date)} is mandatory");

                DateTimeOffset dateoffset;
                if (!DateTimeOffset.TryParse(date, out dateoffset))
                {
                    throw new ArgumentException($"{nameof(date)} is not in proper format");
                }

                return   Ok(await Service.GetUnReadReminderByDate(entityType, entityId, null, dateoffset));
            }
            catch (ArgumentException exception)
            {
                ErrorLog.Warn(exception.Message, new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (Exception ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }

        }

        /// <summary>
        /// GetUnReadReminderByDate
        /// </summary>
        /// <param name="entityId">entityId</param>
        /// <param name="entityType">entityType</param>
        /// <param name="userName">userName</param>
        /// <param name="date">date</param>
        /// <returns>IReminderModel[]</returns>
        [HttpGet("/{entityType}/{entityId}/{userName}/unread/date/{date}")]
#if DOTNET2
        [ProducesResponseType(typeof(IEnumerable<IReminderModel>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> GetUnReadReminderByDate(string entityType, string entityId, string userName, string date)
        {
            try
            {
                if (string.IsNullOrEmpty(date))
                    throw new ArgumentException($"{nameof(date)} is mandatory");

                DateTimeOffset dateoffset;
                if (!DateTimeOffset.TryParse(date, out dateoffset))
                {
                    throw new ArgumentException($"{nameof(date)} is not in proper format");
                }

                return Ok(await Service.GetUnReadReminderByDate(entityType, entityId, userName, dateoffset));
            }
            catch (ArgumentException exception)
            {
                ErrorLog.Warn(exception.Message, new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (Exception ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }

        }

        /// <summary>
        /// GetUnReadReminder
        /// </summary>
        /// <param name="entityId">entityId</param>
        /// <param name="entityType">entityType</param>
        /// <returns>IReminderModel[]</returns>
        [HttpGet("/{entityType}/{entityId}/unread/all")]
#if DOTNET2
        [ProducesResponseType(typeof(IEnumerable<IReminderModel>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> GetUnReadReminder(string entityType, string entityId)
        {
            try
            {
                return Ok(await Service.GetUnReadReminder(entityType, entityId, null));
            }
            catch (ArgumentException exception)
            {
                ErrorLog.Warn(exception.Message, new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (Exception ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }

        }

        /// <summary>
        /// GetUnReadReminder
        /// </summary>
        /// <param name="entityId">entityId</param>
        /// <param name="entityType">entityType</param>
        /// <param name="userName">userName</param>
        /// <returns>IReminderModel[]</returns>
        [HttpGet("/{entityType}/{entityId}/{userName}/unread/all")]
#if DOTNET2
        [ProducesResponseType(typeof(IEnumerable<IReminderModel>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> GetUnReadReminder(string entityType, string entityId, string userName)
        {
            try
            {
                return Ok(await Service.GetUnReadReminder(entityType, entityId, userName));
            }
            catch (ArgumentException exception)
            {
                ErrorLog.Warn(exception.Message, new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (Exception ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }

        }

        /// <summary>
        /// GetUnReadReminderCount
        /// </summary>
        /// <param name="entityId">entityId</param>
        /// <param name="entityType">entityType</param>
        /// <returns>int</returns>
        [HttpGet("/{entityType}/{entityId}/unread/count")]
#if DOTNET2
        [ProducesResponseType(typeof(int), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> GetUnReadReminderCount(string entityType, string entityId)
        {
            try
            {
                var count = await Service.GetUnReadReminderCount(entityType, entityId, null);
                return Ok(count);
            }
            catch (ArgumentException exception)
            {
                ErrorLog.Warn(exception.Message, new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (Exception ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }

        }

        /// <summary>
        /// GetUnReadReminderCount
        /// </summary>
        /// <param name="entityId">entityId</param>
        /// <param name="entityType">entityType</param>
        /// <param name="userName">userName</param>
        /// <returns>int</returns>
        [HttpGet("/{entityType}/{entityId}/{userName}/unread/count")]
#if DOTNET2
        [ProducesResponseType(typeof(int), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> GetUnReadReminderCount(string entityType, string entityId, string userName)
        {

            try
            {
                var count = await Service.GetUnReadReminderCount(entityType, entityId, userName);
                return Ok(count);
            }
            catch (ArgumentException exception)
            {
                ErrorLog.Warn(exception.Message, new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (Exception ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }

        }

        /// <summary>
        /// MarkAsRead
        /// </summary>
        /// <param name="entityId">entityId</param>
        /// <param name="entityType">entityType</param>
        /// <param name="ids">ids</param>
        /// <returns>IReminderModel[]</returns>
        [HttpPost("/{entityType}/{entityId}/mark-as-read")]
#if DOTNET2
        [ProducesResponseType(typeof(IEnumerable< IReminderModel>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async  Task<IActionResult> MarkAsRead(string entityType, string entityId, [FromBody]string[] ids)
        {
            try
            {
                return Ok(await Service.MarkAsRead(entityType, entityId, null, ids));
            }
            catch (ArgumentException exception)
            {
                ErrorLog.Warn(exception.Message, new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (Exception ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }

        }

        /// <summary>
        /// MarkAsRead
        /// </summary>
        /// <param name="entityId">entityId</param>
        /// <param name="entityType">entityType</param>
        /// <param name="userName">userName</param>
        /// <param name="ids">ids</param>
        /// <returns>IReminderModel[]</returns>
        [HttpPost("/{entityType}/{entityId}/{userName}/mark-as-read")]
#if DOTNET2
        [ProducesResponseType(typeof(IEnumerable< IReminderModel>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> MarkAsRead(string entityType, string entityId, string userName, [FromBody]string[] ids)
        {
            try
            {
                return Ok(await Service.MarkAsRead(entityType, entityId, userName, ids));
            }
            catch (ArgumentException exception)
            {
                ErrorLog.Warn(exception.Message, new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (Exception ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }

        }

        /// <summary>
        /// MarkAllAsRead
        /// </summary>
        /// <param name="entityId">entityId</param>
        /// <param name="entityType">entityType</param>
        /// <returns></returns>
        [HttpPut("/{entityType}/{entityId}/mark-all-as-read")]
#if DOTNET2
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task< IActionResult> MarkAllAsRead(string entityType, string entityId)
        {
            try { 
              await Service.MarkAllAsRead(entityType, entityId, null);
                return NoContentResult;
        }
            catch (ArgumentException exception)
            {
                ErrorLog.Warn(exception.Message, new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (Exception ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// MarkAllAsRead
        /// </summary>
        /// <param name="entityId">entityId</param>
        /// <param name="entityType">entityType</param>
        /// <param name="userName">userName</param>
        /// <returns></returns>
        [HttpPut("/{entityType}/{entityId}/{userName}/mark-all-as-read")]
#if DOTNET2
        [ProducesResponseType(typeof(NoContentResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> MarkAllAsRead(string entityType, string entityId, string userName)
        {
            try
            {
                await
                    Service.MarkAllAsRead(entityType, entityId, userName);
                return NoContentResult;
            }
            catch (ArgumentException exception)
            {
                ErrorLog.Warn(exception.Message, new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (Exception ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }
        }

        #region "user specific end point"

        /// <summary>
        /// GetAll
        /// </summary>
        /// <param name="userName">userName</param>
        /// <returns>IReminderModel[]</returns>
        [HttpGet("/{userName}")]
#if DOTNET2
        [ProducesResponseType(typeof(IEnumerable<IReminderModel>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> GetAll(string userName)
        {
            try
            {
                return Ok(await Service.GetAll(userName));
            }
            catch (ArgumentException exception)
            {
                ErrorLog.Warn(exception.Message, new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (Exception ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }

        }

        /// <summary>
        /// GetByDate
        /// </summary>
        /// <param name="userName">userName</param>
        /// <param name="date">date</param>
        /// <returns>IReminderModel[]</returns>
        [HttpGet("/{userName}/date/{date}")]
#if DOTNET2
        [ProducesResponseType(typeof(IEnumerable< IReminderModel>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async  Task<IActionResult> GetByDate(string userName, string date)
        {
            try
            {
                if (string.IsNullOrEmpty(date))
                    throw new ArgumentException($"{nameof(date)} is mandatory");

                DateTimeOffset dateoffset;
                if (!DateTimeOffset.TryParse(date, out dateoffset))
                {
                    throw new ArgumentException($"{nameof(date)} is not in proper format");
                }

                return  Ok(await Service.GetByDate(userName, dateoffset));
            }
            catch (ArgumentException exception)
            {
                ErrorLog.Warn(exception.Message, new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (Exception ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }

        }

        /// <summary>
        /// GetUnReadReminder
        /// </summary>
        /// <param name="userName">userName</param>
        /// <returns>IReminderModel[]</returns>
        [HttpGet("/{userName}/unread/all")]
#if DOTNET2
        [ProducesResponseType(typeof(IEnumerable< IReminderModel>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> GetUnReadReminder(string userName)
        {
            try
            {
                return Ok(await Service.GetUnReadReminder(userName));
            }
            catch (ArgumentException exception)
            {
                ErrorLog.Warn(exception.Message, new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (Exception ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }

        }

        /// <summary>
        /// GetUnReadReminderCount
        /// </summary>
        /// <param name="userName">userName</param>
        /// <returns>int</returns>
        [HttpGet("/{userName}/unread/count")]
#if DOTNET2
        [ProducesResponseType(typeof(int), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task< IActionResult> GetUnReadReminderCount(string userName)
        {
            try
            {
                var count =await Service.GetUnReadReminderCount(userName);
                return Ok(count);
            }
            catch (ArgumentException exception)
            {
                ErrorLog.Warn(exception.Message, new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (Exception ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }


        }

        /// <summary>
        /// MarkAsRead
        /// </summary>
        /// <param name="userName">userName</param>
        /// <param name="ids">ids</param>
        /// <returns>IReminderModel</returns>
        [HttpPost("/{userName}/mark-as-read")]
#if DOTNET2
        [ProducesResponseType(typeof(IReminderModel), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async  Task<IActionResult> MarkAsRead(string userName, [FromBody]string[] ids)
        {
            try
            {
                return Ok(await Service.MarkAsRead(userName, ids));
            }
            catch (ArgumentException exception)
            {
                ErrorLog.Warn(exception.Message, new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (Exception ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }

        }

        /// <summary>
        /// MarkAllAsRead
        /// </summary>
        /// <param name="userName">userName</param>
        /// <returns></returns>
        [HttpPut("/{userName}/mark-all-as-read")]
#if DOTNET2
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> MarkAllAsRead(string userName)
        {
            try
            {
                await
                    Service.MarkAllAsRead(userName);
                return NoContentResult;
            }
            catch (ArgumentException exception)
            {
                ErrorLog.Warn(exception.Message, new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (Exception ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }

        }

        #endregion       

    }
}
