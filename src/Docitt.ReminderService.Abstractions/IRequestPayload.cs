﻿namespace Docitt.ReminderService
{
    public interface IRequestPayload
    {  
        string Description { get; set; }        
    }
}
