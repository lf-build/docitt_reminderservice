﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.ReminderService.Services
{
    public interface IReminderService
    {
        Task  Add(string entityType, string entityId, string userName, IRequestPayload reminderData);
        Task<IEnumerable<IReminderModel>> GetAll(string entityType, string entityId, string userName);
        Task<IEnumerable<IReminderModel>> GetByDate(string entityType, string entityId, string userName, DateTimeOffset date);
        Task<IEnumerable<IReminderModel>> GetUnReadReminderByDate(string entityType, string entityId, string userName, DateTimeOffset date);
        Task<IEnumerable<IReminderModel>> GetUnReadReminder(string entityType, string entityId, string userName);
       Task< int> GetUnReadReminderCount(string entityType, string entityId, string userName);
        Task<IEnumerable<IReminderModel>> MarkAsRead(string entityType, string entityId, string userName, IEnumerable<string> reminderIds);
        Task MarkAllAsRead(string entityType, string entityId, string userName);

        Task<IEnumerable<IReminderModel>> GetAll(string userName);
        Task<IEnumerable<IReminderModel>> GetByDate(string userName, DateTimeOffset date);
        Task<IEnumerable<IReminderModel>> GetUnReadReminder(string userName);

       Task< int> GetUnReadReminderCount(string userName);

        Task<IEnumerable<IReminderModel>> MarkAsRead(string userName, IEnumerable<string> reminderIds);

        Task MarkAllAsRead(string userName);
    }
}
