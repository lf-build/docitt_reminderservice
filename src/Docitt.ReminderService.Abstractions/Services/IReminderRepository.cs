﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.ReminderService.Services
{
    public interface IReminderRepository : IRepository<IReminderModel>
    {
        Task<IEnumerable<IReminderModel>> GetAll(string entityType, string entityId, string userName);       
        Task<IEnumerable<IReminderModel>> GetUnReadReminder(string entityType, string entityId, string userName);
        Task<IEnumerable<IReminderModel>> GetUnReadReminderByDate(string entityType, string entityId, string userName, DateTimeOffset date);
        Task<IEnumerable<IReminderModel>> GetByDate(string entityType, string entityId, string userName, DateTimeOffset date);
        void MarkAsRead(string entityType, string entityId, string userName, IEnumerable<string> reminderIds);
        void MarkAllAsRead(string entityType, string entityId, string userName);

        Task<IEnumerable<IReminderModel>> GetAll(string userName);
        Task<IEnumerable<IReminderModel>> GetByDate(string userName, DateTimeOffset date);

        Task<IEnumerable<IReminderModel>> GetUnReadReminder(string userName);

        void MarkAsRead( string userName, IEnumerable<string> reminderIds);

        void MarkAllAsRead(string userName);
    }
}