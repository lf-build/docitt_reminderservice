﻿using LendFoundry.Foundation.Persistence;
using System;

namespace Docitt.ReminderService
{
    public interface IReminderModel : IAggregate
    {
        string EntityId { get; set; }
        string EntityType { get; set; }
        string UserName { get; set; }
        DateTimeOffset CreatedDate { get; set; }
        string Description { get; set; }
        bool isRead { get; set; }
    }
}