﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;

namespace Docitt.ReminderService
{
    public class Configuration : IDependencyConfiguration
    {
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
    }
}
