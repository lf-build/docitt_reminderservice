﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.ReminderService.Events
{
    public class ReminderRequestCompleted
    {
        public IReminderModel Request { get; set; }
    }
}
