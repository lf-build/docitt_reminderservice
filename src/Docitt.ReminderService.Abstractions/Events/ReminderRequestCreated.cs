﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.ReminderService.Events
{
    public class ReminderRequestCreated
    {
        public IReminderModel Request { get; set; }
    }
}
