﻿namespace Docitt.ReminderService
{
    public class RequestPayload : IRequestPayload
    {
        public string Description { get; set; }

    }
}
