﻿using LendFoundry.Foundation.Persistence;
using System;
namespace Docitt.ReminderService
{
    public class ReminderModel : Aggregate,IReminderModel
    {
        public string EntityType { get; set; }
        public string EntityId { get; set; }
        public string UserName { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
        public string Description { get; set; }
       public bool isRead { get; set; }
    }
}
