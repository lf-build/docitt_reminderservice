﻿using System;

namespace Docitt.ReminderService.Abstractions
{
        public static class Settings
        {

            public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "reminders";

        }
    
}
