﻿using Docitt.ReminderService.Services;
using LendFoundry.Foundation.Client;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.ReminderService.Client
{
    public class ReminderServicesClient : IReminderService
    {
        public ReminderServicesClient(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }


        public async Task Add(string entityType, string entityId, IRequestPayload request)
        {
              await Client.PostAsync<IRequestPayload>($"/{entityType}/{entityId}/", request, true);
        }

        public async Task Add(string entityType, string entityId, string userName, IRequestPayload request)
        {
            if (string.IsNullOrWhiteSpace(userName))
                userName = string.Empty;

         await Client.PostAsync<IRequestPayload>($"/{entityType}/{entityId}/{userName}", request, true);
        }

        public async Task<IEnumerable<IReminderModel>> GetAll(string userName)
        {
            return await Client.GetAsync<List<ReminderModel>>($"/{userName}");
        }

        public async Task<IEnumerable<IReminderModel>> GetAll(string entityType, string entityId)
        {
            return await Client.GetAsync<List<ReminderModel>>($"/{entityType}/{entityId}/");
        }

        public async Task<IEnumerable<IReminderModel>> GetAll(string entityType, string entityId, string userName)
        {
            return await Client.GetAsync<List<ReminderModel>>($"/{entityType}/{entityId}/{userName}");
        }

        public async Task<IEnumerable<IReminderModel>> GetByDate(string userName, DateTimeOffset date)
        {
            string dateString = Convert.ToString(date.Date);
            return await Client.PostAsync<dynamic, List<ReminderModel>>($"/{userName}/date/{dateString}", null, true);
        }

        public async Task<IEnumerable<IReminderModel>> GetByDate(string entityType, string entityId, DateTimeOffset date)
        {
            string dateString = Convert.ToString(date.Date);
            return await Client.PostAsync<dynamic, List<ReminderModel>>($"/{entityType}/{entityId}/date/{dateString}", null, true);
        }

        public async Task<IEnumerable<IReminderModel>> GetByDate(string entityType, string entityId, string userName, DateTimeOffset date)
        {
             string dateString = Convert.ToString(date.Date);
            return await Client.PostAsync<dynamic, List<ReminderModel>>($"/{entityType}/{entityId}/{userName}/date/{dateString}", null, true);
        }

        public async Task<IEnumerable<IReminderModel>> GetUnReadReminder(string userName)
        {
             return await Client.GetAsync<List<ReminderModel>>($"/{userName}/unread/all");
        }

        public async Task<IEnumerable<IReminderModel>> GetUnReadReminder(string entityType, string entityId)
        {
            return await Client.GetAsync<List<ReminderModel>>($"/{entityType}/{entityId}");
        }

        public async Task<IEnumerable<IReminderModel>> GetUnReadReminder(string entityType, string entityId, string userName)
        {
               return await Client.GetAsync<List<ReminderModel>>($"/{entityType}/{entityId}/{userName}/unread/all");
        }

        public async Task<IEnumerable<IReminderModel>> GetUnReadReminderByDate(string entityType, string entityId, DateTimeOffset date)
        {
            string dateString = Convert.ToString(date.Date);
            return await Client.PostAsync<dynamic, List<ReminderModel>>($"/{entityType}/{entityId}/date/{dateString}", null, true);
        }

        public async Task<IEnumerable<IReminderModel>> GetUnReadReminderByDate(string entityType, string entityId, string userName, DateTimeOffset date)
        {
            string dateString = Convert.ToString(date.Date);
            return await Client.PostAsync<dynamic, List<ReminderModel>>($"/{entityType}/{entityId}/{userName}/unread/date/{dateString}", null, true);
        }

        public async Task<int> GetUnReadReminderCount(string userName)
        {
            var response = await Client.GetAsync<dynamic>($"/{userName}/unread/count");
            return Convert.ToInt32(response);
        }

        public async Task<int> GetUnReadReminderCount(string entityType, string entityId)
        {
              var response = await Client.GetAsync<dynamic>($"/{entityType}/{entityId}/unread/count");
            return Convert.ToInt32(response);
        }

        public async Task< int> GetUnReadReminderCount(string entityType, string entityId, string userName)
        {
            var response = await Client.GetAsync<dynamic>($"/{entityType}/{entityId}/{userName}/unread/count");
            return Convert.ToInt32(response);
        }

        public async Task MarkAllAsRead(string userName)
        {
           await Client.PutAsync<dynamic>($"/{userName}/mark-all-as-read", null, true);
        }

        public async Task MarkAllAsRead(string entityType, string entityId)
        {
           await Client.PutAsync<dynamic>($"/{entityType}/{entityId}/mark-all-as-read", null, true);
        }

        public async Task MarkAllAsRead(string entityType, string entityId, string userName)
        {
           await Client.PutAsync<dynamic>($"/{entityType}/{entityId}/{userName}/mark-all-as-read", null, true);
        }

        public async Task<IEnumerable<IReminderModel>> MarkAsRead(string userName, IEnumerable<string> reminderIds)
        {
             return await Client.PostAsync<IEnumerable<string>, List<ReminderModel>>($"/{userName}/mark-as-read", reminderIds, true);
        }

        public async Task<IEnumerable<IReminderModel>> MarkAsRead(string entityType, string entityId, IEnumerable<string> reminderIds)
        {
            return await Client.PostAsync<IEnumerable<string>, List<ReminderModel>>($"/{entityType}/{entityId}/mark-as-read", reminderIds, true);
        }

        public async Task<IEnumerable<IReminderModel>> MarkAsRead(string entityType, string entityId, string userName, IEnumerable<string> reminderIds)
        {
            return await Client.PostAsync<IEnumerable<string>, List<ReminderModel>>($"/{entityType}/{entityId}/{userName}/mark-as-read", reminderIds, true);
        }
    }
}
