﻿using Docitt.ReminderService.Services;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
namespace Docitt.ReminderService.Client
{
    public class ReminderServicesClientFactory : IReminderServiceClientFactory
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public ReminderServicesClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Uri = new UriBuilder("http", endpoint, port).Uri;
        }

        public ReminderServicesClientFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }
        private IServiceProvider Provider { get; set; }

        private Uri Uri { get; }


        public IReminderService Create(ITokenReader reader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("reminder");
            }


            var client = Provider.GetServiceClient(reader, uri);
            return new ReminderServicesClient(client);
        }

      
    }
}
