﻿using Docitt.ReminderService.Services;
using LendFoundry.Security.Tokens;

namespace Docitt.ReminderService.Client
{
    public interface IReminderServiceClientFactory
    {
        IReminderService Create(ITokenReader reader);
    }
}