﻿using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using System;
namespace Docitt.ReminderService.Client
{
    public static class ReminderServicesClientExtensions
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddReminderService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IReminderServiceClientFactory>(p => new ReminderServicesClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IReminderServiceClientFactory>().Create(p.GetService<ITokenReader>()));

            return services;
        }

        public static IServiceCollection AddReminderService(this IServiceCollection services, Uri uri)
        {
            services.AddTransient<IReminderServiceClientFactory>(p => new ReminderServicesClientFactory(p, uri));
            services.AddTransient(p => p.GetService<IReminderServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddReminderService(this IServiceCollection services)
        {
            services.AddTransient<IReminderServiceClientFactory>(p => new ReminderServicesClientFactory(p));
            services.AddTransient(p => p.GetService<IReminderServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

       
    }
}
