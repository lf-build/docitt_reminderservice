﻿using Docitt.ReminderService.Mocks;
using Docitt.ReminderService;
using Docitt.ReminderService.Services;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Lookup;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using System.Threading.Tasks;

namespace Docitt.ReminderService.Tests
{
    public class ReminderServicesTests
    {
        private FakeReminderRepository reminderRepository { get; }
      
        private Mock<ITenantTime> TenantTime { get; }
        private Mock<IEventHubClient> EventHub { get; }

        private Mock<ILookupService> mockLookUp { get; }
 
      
        private IReminderService Service { get; }

        public ReminderServicesTests()
        {
            reminderRepository = new FakeReminderRepository();

            TenantTime = new Mock<ITenantTime>();
            EventHub = new Mock<IEventHubClient>();
            mockLookUp = new Mock<ILookupService>();
            //Validator = new Mock<IValidatorService>();

            Service = new ReminderServices(reminderRepository,
                TenantTime.Object
                , EventHub.Object,
                mockLookUp.Object);
        }

        [Fact]
        public void Service_Init_With_NullArguments_Throws_ArgumentException()
        {
            Assert.Throws<ArgumentException>(() => new ReminderServices(null, null, null, null));
        }

        [Fact]
        public void Service_Init_With_Valid_Arguments()
        {
            var service = new ReminderServices(reminderRepository,
                TenantTime.Object
                , EventHub.Object,
                mockLookUp.Object);

            Assert.NotNull(service);
        }

        [Fact]
        public  void  Service_Add_With_NULLOREMPTY_EntityId_EntityType_Throws_ArgumentException()
        {
            Assert.ThrowsAsync<ArgumentException>(async () => await  Service.Add(null, "12345", It.IsAny<string>(), new RequestPayload()));
            Assert.ThrowsAsync<ArgumentException>(async () => await  Service.Add("application", "", It.IsAny<string>(), new RequestPayload()));
            Assert.ThrowsAsync<ArgumentException>(async () => await  Service.Add("application", "12345", It.IsAny<string>(),new RequestPayload()));
        }

        [Fact]
        public void Service_Add_With_NULL_RequestPayload_Throws_ArgumentException()
        {
            Assert.ThrowsAsync<ArgumentException>(async () => await  Service.Add("application", "12345", It.IsAny<string>(),null));
        }

        [Fact]
        public void Service_Add_Valid_Arguments_Returns()
        {
            mockLookUp.Setup(s => s.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(new Dictionary<string, string>
                {
                    { "loan","loan" },
                    { "merchant","merchant" },
                    { "application","application" },
                    { "borrower","borrower" },
                });

            EventHub.Setup(x => x.Publish(It.IsAny<string>(), It.IsAny<object>()))
                .ReturnsAsync(true);

           Service.Add("application", "000001", It.IsAny<string>(),new RequestPayload {Description="Test Title"});


          
            EventHub.Verify(x => x.Publish(It.IsAny<string>(), It.IsAny<object>()), Times.AtLeastOnce);

            
        }

        


        [Fact]
        public async Task  Service_GetAll_Events()
        {
            var result = await Service.GetAll("application", "000001", It.IsAny<string>());

            Assert.NotNull(result);
        }



        [Fact]
        public async Task  Service_GetByDate_Events()
        {
            reminderRepository.Add(new ReminderModel { EntityType = "application", EntityId = "000001", Description = "test", CreatedDate = new DateTimeOffset(new DateTime(2017, 4, 5, 0, 0, 0)) });
            reminderRepository.Add(new ReminderModel { EntityType = "application", EntityId = "000001", Description = "test", CreatedDate = new DateTimeOffset(DateTime.Now) });

            var result = await Service.GetByDate("application", "000001", It.IsAny<string>(), DateTime.UtcNow.Date);

            Assert.NotNull(result);
          
        }

    }
}
