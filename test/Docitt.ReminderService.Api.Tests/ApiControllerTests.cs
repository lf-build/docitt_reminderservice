﻿using System;
using System.Collections.Generic;
using Microsoft.AspNet.Mvc;
using Moq;
using Xunit;
using Docitt.ReminderService.Api.Controllers;
using LendFoundry.Foundation.Services;
using LendFoundry.Foundation.Logging;
using System.Threading.Tasks;

namespace Docitt.ReminderService.Api.Tests
{
    public class ApiControllerTests
    {
        private Mock<Services.IReminderService> ReminderServices { get; }
        private Mock<ILogger> Logger { get; }
        private ApiController Api { get; }

        public ApiControllerTests()
        {
            ReminderServices = new Mock<Services.IReminderService>();
            Logger = new Mock<ILogger>();
            Api = new ApiController(ReminderServices.Object, Logger.Object);
        }

        [Fact]
        public void Controller_With_Null_Services_Should_Throw_ArgumentException()
        {
            Assert.Throws<ArgumentException>(() => new ApiController(null,null));
        }

        [Fact]
        public async Task Controller_ADD_With_Null_EntityType_EntityId_Should_Thorw_ArgumentException()
        {
            ReminderServices.Setup(x => x.Add(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<RequestPayload>())).Throws(new ArgumentException("entityType is mandatory"));

            var result = (ErrorResult)await  Api.Add("", "12345",new RequestPayload());
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);
            ReminderServices.Setup(x => x.Add(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<RequestPayload>())).Throws(new ArgumentException("entityid is mandatory"));

            result = (ErrorResult)await Api.Add("application", "", new RequestPayload());
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);           
        }

        [Fact]
        public async Task Controller_ADD_With_Null_Payload_Should_Thorw_ArgumentException()
        {
            RequestPayload payload = null;
            ReminderServices.Setup(x => x.Add(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<RequestPayload>())).Throws(new ArgumentException("request is mandatory"));
            var result = (ErrorResult) await Api.Add("application", "12345", payload);
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);
        }


        [Fact]
        public async Task Controller_ADD_Returns_Result_OnSuccess()
        {
            
            var payload = new RequestPayload
            {
                Description = "Payment Due"              
            };

            var result = (HttpStatusCodeResult)await  Api.Add("application","12345", payload);

            Assert.NotNull(result);
            Assert.Equal(204, result.StatusCode);
        }


        [Fact]
        public async Task  Controller_GetAll_With_Null_EnityId_EntityType_Throw_ArgumentException()
        {
            ReminderServices.Setup(x => x.GetAll(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Throws(new ArgumentException("entitytype is mandatory"));
            var result = (ErrorResult)await Api.GetAll("", "12345");

            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);


            ReminderServices.Setup(x => x.GetAll(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Throws(new ArgumentException("entityid is mandatory"));
            result = (ErrorResult)await Api.GetAll("application", "");

            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);
        }


        [Fact]
        public async Task  Controller_GetAllEvents_Returns_ErrorResult_OnFailure()
        {
            ReminderServices.Setup(x => x.GetAll(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .ThrowsAsync(new InvalidOperationException("Unknown exception"));

            var result = (ErrorResult)await Api.GetAll("application", "000001");

            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task  Controller_GetAllEvents_Returns_Result_OnSuccess()
        {
            ReminderServices.Setup(x => x.GetAll(It.IsAny<string>(), It.IsAny<string>(),It.IsAny<string>()))
                .ReturnsAsync(new List<ReminderModel>());

            var result = (HttpOkObjectResult)await Api.GetAll("application", "jondoe");

            Assert.NotNull(result);
            Assert.Equal(200, result.StatusCode);
            Assert.IsType<List<ReminderModel>>(result.Value);
        }

        //[Fact]
        //public async void Controller_GetByMonth_Returns_Result_OnSuccess()
        //{
        //    ReminderServices.Setup(x => x.GetByMonth(It.IsAny<string>(), It.IsAny<string>(),It.IsAny<int>(),It.IsAny<int>()))
        //        .ReturnsAsync(new List<ReminderModel>());

        //    var result = (HttpOkObjectResult)await Api.GetByMonth("application", "jondoe",4,2017);

        //    Assert.NotNull(result);
        //    Assert.Equal(200, result.StatusCode);
        //    Assert.IsType<List<ReminderModel>>(result.Value);
        //}

        [Fact]
        public async Task  Controller_GetByDate_Returns_Result_OnSuccess()
        {
            ReminderServices.Setup(x => x.GetByDate(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<DateTimeOffset>()))

                .ReturnsAsync(new List<ReminderModel>());
           

            var result = (HttpOkObjectResult)await Api.GetByDate("application", "12345", DateTime.UtcNow.Date.ToString());

            Assert.NotNull(result);
            Assert.Equal(200, result.StatusCode);
            Assert.IsType<List<ReminderModel>>(result.Value);
        }
    }
}
