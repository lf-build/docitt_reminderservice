﻿using Docitt.ReminderService;
using Docitt.ReminderService.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Docitt.ReminderService.Mocks
{
    public class FakeReminderRepository : IReminderRepository
    {
        public List<IReminderModel> EventInMemoryData { get; set; } = new List<IReminderModel>();
        public FakeReminderRepository()
        {
        }

        public void Add(IReminderModel item)
        {
            item.Id = Guid.NewGuid().ToString("N");
            EventInMemoryData.Add(item);
        }

        public Task<IEnumerable<IReminderModel>> All(Expression<Func<IReminderModel, bool>> query, int? skip = default(int?), int? quantity = default(int?))
        {
            return Task.Run(() =>
            {
                return EventInMemoryData
                    .AsQueryable()
                    .Where(query)
                    .AsEnumerable();
            });
        }

        public int Count(Expression<Func<IReminderModel, bool>> query)
        {
            return EventInMemoryData
                .AsQueryable()
                .Count(query);
        }

        public Task<IEnumerable<IReminderModel>> GetAll(string entityType, string entityId)
        {
            return Task.FromResult<IEnumerable<IReminderModel>>(EventInMemoryData.AsEnumerable());
        }
     
        public Task<IEnumerable<IReminderModel>> GetByDate(string entityType, string entityId, DateTimeOffset date)
        {
            return Task.FromResult<IEnumerable<IReminderModel>>(EventInMemoryData.Where(p => p.EntityType == entityType && p.EntityId == entityId && p.CreatedDate >= date).ToList());
        }

        public  Task<IEnumerable<IReminderModel>> GetUnReadReminderByDate(string entityType, string entityId, DateTimeOffset date)
        {
            return Task.FromResult<IEnumerable<IReminderModel>>(EventInMemoryData.Where(p => p.EntityType == entityType && p.EntityId == entityId && !p.isRead && p.CreatedDate == date).ToList());
        }

        public Task<IEnumerable<IReminderModel>> GetUnReadReminder(string entityType, string entityId)
        {            
            return Task.FromResult<IEnumerable<IReminderModel>>(EventInMemoryData.Where(p => p.EntityType == entityType && p.EntityId == entityId && !p.isRead).ToList());
        }

        public void MarkAsRead(string entityType, string entityId, IEnumerable<string> reminderIds)
        {   
            EventInMemoryData.Where(x => x.EntityType == entityType && x.EntityId == entityId && reminderIds.Contains(x.Id)).ToList().ForEach(c => c.isRead = true);
        }

        public void MarkAllAsRead(string entityType, string entityId)
        {
            EventInMemoryData.Where(x => x.EntityType == entityType && x.EntityId == entityId ).ToList().ForEach(c => c.isRead = true);
        }
        public void Remove(IReminderModel item)
        {
            throw new NotImplementedException();
        }

        public void Update(IReminderModel item)
        {
            throw new NotImplementedException();
        }

        public Task<IReminderModel> Get(string id)
        {
            return Task.FromResult<IReminderModel>(EventInMemoryData.Where(p => p.Id == id).FirstOrDefault());
        }

        public Task<IEnumerable<IReminderModel>> GetAll(string entityType, string entityId, string userName)
        {
            return Task.FromResult<IEnumerable<IReminderModel>>(EventInMemoryData.Where(p => p.EntityType == entityType && p.EntityId == entityId && p.UserName == userName).ToList());
        }

        public Task<IEnumerable<IReminderModel>> GetUnReadReminder(string entityType, string entityId, string userName)
        {
            return Task.FromResult<IEnumerable<IReminderModel>>(EventInMemoryData.Where(p => p.EntityType == entityType && p.EntityId == entityId && !p.isRead && p.UserName == userName).ToList());
        }

        public Task<IEnumerable<IReminderModel>> GetUnReadReminderByDate(string entityType, string entityId, string userName, DateTimeOffset date)
        {
            return Task.FromResult<IEnumerable<IReminderModel>>(EventInMemoryData.Where(p => p.EntityType == entityType && p.EntityId == entityId && !p.isRead && p.CreatedDate == date && p.UserName == userName).ToList());
        }

        public Task<IEnumerable<IReminderModel>> GetByDate(string entityType, string entityId, string userName, DateTimeOffset date)
        {
            return Task.FromResult<IEnumerable<IReminderModel>>(EventInMemoryData.Where(p => p.EntityType == entityType && p.EntityId == entityId && p.CreatedDate >= date && p.UserName == userName).ToList());
        }

        public void MarkAsRead(string entityType, string entityId, string userName, IEnumerable<string> reminderIds)
        {
            EventInMemoryData.Where(x => x.EntityType == entityType && x.EntityId == entityId && x.UserName == userName && reminderIds.Contains(x.Id)).ToList().ForEach(c => c.isRead = true);
        }

        public void MarkAllAsRead(string entityType, string entityId, string userName)
        {
            EventInMemoryData.Where(x => x.EntityType == entityType && x.EntityId == entityId && x.UserName == userName).ToList().ForEach(c => c.isRead = true);
        }

        public Task<IEnumerable<IReminderModel>> GetAll(string userName)
        {
            return Task.FromResult<IEnumerable<IReminderModel>>(EventInMemoryData.Where(p => p.UserName == userName).ToList());
        }

        public Task<IEnumerable<IReminderModel>> GetByDate(string userName, DateTimeOffset date)
        {
            return Task.FromResult<IEnumerable<IReminderModel>>(EventInMemoryData.Where(p => p.CreatedDate >= date && p.UserName == userName).ToList());
        }

        public Task<IEnumerable<IReminderModel>> GetUnReadReminder(string userName)
        {
            return Task.FromResult<IEnumerable<IReminderModel>>(EventInMemoryData.Where(p => !p.isRead && p.UserName == userName).ToList());
        }

        public void MarkAsRead(string userName, IEnumerable<string> reminderIds)
        {
            EventInMemoryData.Where(x => x.UserName == userName && reminderIds.Contains(x.Id)).ToList().ForEach(c => c.isRead = true);
        }

        public void MarkAllAsRead(string userName)
        {
            EventInMemoryData.Where(x => x.UserName == userName).ToList().ForEach(c => c.isRead = true);
        }
    }
}
