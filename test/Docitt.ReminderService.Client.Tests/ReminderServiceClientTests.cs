﻿using System.Collections.Generic;
using LendFoundry.Foundation.Services;
using Moq;
using RestSharp;
using Xunit;
using Docitt.ReminderService.Services;
using Docitt.ReminderService;
using System;
using System.Linq;
using LendFoundry.Foundation.Client;

namespace Docitt.ReminderService.Client.Tests
{
    public class ReminderServiceClientTests
    {
        private Mock<IServiceClient> ServiceClient { get; }
        private IReminderService Client { get; }
        private IRestRequest Request { get; set; }

        public ReminderServiceClientTests()
        {
            ServiceClient = new Mock<IServiceClient>();
            Client = new Docitt.ReminderService.Client.ReminderServicesClient(ServiceClient.Object);
        }

        [Fact]
        public void Client_Add()
        {
            ServiceClient.Setup(s => s.Execute(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
             .Returns(true);

            Client.Add("application", "000001","suresh.b@sigmainfo.net",new RequestPayload());

            ServiceClient.Verify(x => x.Execute(It.IsAny<IRestRequest>()), Times.Once);

            Assert.Equal("/{entityType}/{entityId}/", Request.Resource);
            Assert.Equal(Method.POST, Request.Method);
        }

        [Fact]
        public async void Client_GetAll()
        {
            ServiceClient.Setup(s => s.ExecuteAsync<List<ReminderModel>>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .ReturnsAsync(new List<ReminderModel>());

            var result = await Client.GetAll("application", "000001", "suresh.b@sigmainfo.net");

            ServiceClient.Verify(x => x.ExecuteAsync<List<ReminderModel>>(It.IsAny<IRestRequest>()), Times.Once);

            Assert.Equal("/{entityType}/{entityid}/", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);
        }


      
        [Fact]
        public async void Client_GetByDate()
        {
            ServiceClient.Setup(s => s.ExecuteAsync<List<ReminderModel>>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .ReturnsAsync(new List<ReminderModel>());

            var result = await Client.GetByDate("application", "000001", "suresh.b@sigmainfo.net", DateTime.UtcNow.Date);

            ServiceClient.Verify(x => x.ExecuteAsync<List<ReminderModel>>(It.IsAny<IRestRequest>()), Times.Once);

            Assert.Equal("/{entityType}/{entityId}/date", Request.Resource);
            Assert.Equal(Method.POST, Request.Method);
            Assert.NotNull(result);
        }


        [Fact]
        public async void Client_GetUnReadReminder()
        {
            ServiceClient.Setup(s => s.ExecuteAsync<List<ReminderModel>>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .ReturnsAsync(new List<ReminderModel>());

            var result = await Client.GetUnReadReminder("application", "000001", "suresh.b@sigmainfo.net");

            ServiceClient.Verify(x => x.ExecuteAsync<List<ReminderModel>>(It.IsAny<IRestRequest>()), Times.Once);

            Assert.Equal("/{entityType}/{entityId}", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public async void Client_GetUnReadReminderByDate()
        {
            ServiceClient.Setup(s => s.ExecuteAsync<List<ReminderModel>>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .ReturnsAsync(new List<ReminderModel>());

            var result = await Client.GetUnReadReminderByDate("application", "000001", "suresh.b@sigmainfo.net", DateTime.UtcNow.Date);

            ServiceClient.Verify(x => x.ExecuteAsync<List<ReminderModel>>(It.IsAny<IRestRequest>()), Times.Once);

            Assert.Equal("/{entityType}/{entityId}/date", Request.Resource);
            Assert.Equal(Method.POST, Request.Method);
            Assert.NotNull(result);
        }

        

        [Fact]
        public void Client_MarkAllAsRead()
        {
            ServiceClient.Setup(s => s.Execute(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(true);

             Client.MarkAllAsRead("application", "000001", "suresh.b@sigmainfo.net");

            ServiceClient.Verify(x => x.Execute(It.IsAny<IRestRequest>()), Times.Once);

            Assert.Equal("/{entityType}/{entityId}/markallasread", Request.Resource);
            Assert.Equal(Method.PUT, Request.Method);
            
        }
    }
}
